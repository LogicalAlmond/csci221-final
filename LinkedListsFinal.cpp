#include <iostream>
using namespace std;

struct node{
    int data;
    struct node* next;
    ~node(){
        delete (next);
    }
};

void Push(struct node** headRef, int data){
    struct node* newNode = new struct node;
    newNode->data = data;
    newNode->next = *headRef;
    *headRef = newNode;
}
void Print(struct node* head){
    struct node* current = head;
    while(current!= NULL){
        cout << current->data << endl;
        current = current->next;
    }
}

// int Length(struct node* head){
//     struct node* current = head;
//     int counter = 0;
//     while(current!= NULL){
//         //cout << current->data << endl;
//         counter++;
//         current = current->next;
//     }
//     return counter;
// }

// void PrintRecursive(struct node* head){
//     if(head == NULL) return;
//     cout << head->data << endl;
//     PrintRecursive(head->next);
// }

// void AddToEnd(struct node** headRef, int data){
//     struct node* current = *headRef;
//     if(current == NULL){
//         Push(headRef, data);
//     }else{
//     while(current->next != NULL){
//         current = current->next;
//     }
//     Push(&(current->next), data);
//     }
// }
// struct node* FindFromTheEnd(struct node* head, int k){
//     struct node* current = head;
//     struct node* runner = head;
//     if(k>Length(head)){
//         return NULL;
//     }
//     for(int i=0; i<k; i++){
//         runner = runner->next;
//     }
//     while(runner != NULL){
//         current = current->next;
//         runner = runner->next;
//     }
//     return current;
// }

void InsertSorted(struct node** headRef, int data){
    struct node* newNode = new struct node;
    newNode->data = data;
    struct node* current = *headRef;
    if(current == NULL || (*headRef)->data >= data){
        newNode->next = *headRef;
        *headRef = newNode;
    }else{
        while(current->next != NULL && current->next->data < data){
        current = current->next;
        }
    newNode->next = current->next;
    current->next = newNode;
    }
}

/* Final questions below
    Video URL: https://www.youtube.com/watch?v=A-F0K2KVwuE
*/
// Question 1.A
bool removeNode(struct node** headRef, int target){
    // create a current pointer and previous pointer to keep track of nodes
    node* current = *headRef;
    node* previous = NULL;

    // If first node is NULL, list is empty -> return false
    if (current == NULL) return false;

    if (current->next == NULL && current->data != target) return false;

    // If first node contains the target to be deleted
    if (current->data == target){ // if the current data is equal to the target to delete
        *headRef = current->next; // then set the pointer to the head equal to the next data
        delete current; // then free the temp variable
        return true;
    }
    else{
        // while current data does not equal the target (means node is either in the internal or end node)
        while (current->data != target){
            previous = current; // set previous to current node
            current = current->next; // then advance current to the next node
            if (current->next == NULL && current->data != target){
                return false;
            }
        }

        previous->next = current->next; // finally, once the current-> data == target, then set previous->next to NULL

        delete current; // free temp
        return true;
    }
}

// // Question 1.B
bool removeDuplicatesSorted(struct node* headref){
    // // Create temps
    node* current = headref;
    node* temp = NULL;

    // If current == NULL, list empty, return
    if (current == NULL) return false;

    // while we have not reached the end of the list
    while (current->next != NULL){
        if (current->data == current->next->data){ // Check if current data is equal to the next data
            temp = current->next->next; // then set temp to the next of the next
            current->next = temp; //set new current next to temp node
            return true;
        }
        else{
            current = current->next;
        }
    }
    return false;
}

// Question 2.A
bool removeDuplicatesUnsorted(struct node* headRef){
    node* current = headRef;
    node* runner = NULL;
    node* temp = NULL;


    while (current != NULL && current->next != NULL){
        runner = current;

        while (runner->next != NULL){
            if (current->data == runner->next->data){
                temp = runner->next;
                runner->next = runner->next->next;
                return true;
            }
            else{
                runner = runner->next;
            }
        }
        current = current->next;
    }
    return false;
}

// Question 2.B
void Reverse(struct node** headRef){
    node* current = *headRef;
    node* previous = NULL;
    node* nextNode = NULL;

    if (current == NULL) return;

    if (current->next == NULL) return;

    while (current != NULL){
        nextNode = current->next;
        current->next = previous;
        previous = current;
        current = nextNode;
    }
    *headRef = previous;
}


int main() {
    struct node* head = NULL;
    Push(&head, 2);
    Push(&head, 5);
    Push(&head, 5);
    Push(&head, 8);
    // Push(&head, 8);
    // Push(&head, 8);
    // Push(&head, 2);
    Push(&head, 9);
    // // AddToEnd(&head, 4);
    // InsertSorted(&head, 4);
    // InsertSorted(&head, 5);
    // InsertSorted(&head, 2);
    // InsertSorted(&head, 2);
    // InsertSorted(&head, 6);
    // InsertSorted(&head, 1);
    
    
    /*
    Question 1.A
    returns true if the node with the given data is removed, else returns false
    */
    cout << endl << "Question 1A: LINKED LIST BEFORE RemoveNode IS RAN" << endl;
    Print(head);
    removeNode(&head, 4)? cout << endl << "Question 1A: TRUE" << endl : cout << endl << "Question 1A: FALSE" << endl;
    cout << endl << "Question 1A: LINKED LIST AFTER RemoveNode IS RAN" << endl;
    Print(head);


    /*
    Questions 1.B
    returns true if there are nodes with duplicated values and removed successfully in a
    sorted singly linked list, else returns false
    */
    cout << endl << "Question 1B: LINKED LIST BEFORE RemoveDuplicatesSorted IS RAN" << endl;
    Print(head);
    removeDuplicatesSorted(head)? cout << endl << "Question 1B: TRUE" << endl : cout << endl << "Question 1B: FALSE" << endl;
    cout << endl << "Question 1B: LINKED LIST AFTER RemoveDuplicatesSorted IS RAN" << endl;
    Print(head);


    /*
    Question 2.A
    returns true if there are nodes with duplicated values and removed successfully in a
    unsorted singly linked list
    */
    cout << endl << "Question 2A: LINKED LIST BEFORE RemoveDuplicatesUnsorted IS RAN" << endl;
    Print(head);
    removeDuplicatesUnsorted(head)? cout << "Question 2A: TRUE" : cout << "Question 2A: FALSE";
    cout << endl << "Question 2A: LINKED LIST AFTER RemoveDuplicatesUnsorted IS RAN" << endl;
    Print(head);


    /*
    Question 2.B
    reverses the nodes in the singly linked list
    */
    cout << endl << "Question 2B: LINKED LIST BEFORE Reverse IS RAN" << endl;
    Print(head);
    Reverse(&head);
    cout << endl << "Question 2B: LINKED LIST AFTER Reverse IS RAN" << endl;
    Print(head);

    delete (head);

    return 0;
}